package com.example.company.controller;

import com.example.company.model.Employee;
import com.example.company.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/all")
    public ResponseEntity<?> allemps(){
        return employeeService.allemp();
    }

    @GetMapping("/{id}")
    public ResponseEntity getempbyId(@PathVariable String id){
        return employeeService.empbyid(id);
    }
    @PostMapping("/create")
    public ResponseEntity<?> createemp(@RequestBody Employee emp){
        return employeeService.storeemp(emp);
    }

    @PutMapping("/update")
    public ResponseEntity updateemp(@RequestBody Employee emp){
        return employeeService.updateemp(emp);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteemp(@PathVariable String id){
        return employeeService.deleteemp(id);
    }

    @GetMapping("/stat")
    public ResponseEntity checkstat(){
        return employeeService.checkstat();
    }

}
