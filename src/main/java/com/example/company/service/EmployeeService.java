package com.example.company.service;

import com.example.company.model.Employee;
import com.example.company.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeService {

//    @Autowired
    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {

        this.employeeRepository=employeeRepository;
    }


    public ResponseEntity<?> allemp(){
        try{
            return ResponseEntity.ok().body(employeeRepository.findAll());
        }catch (Exception e){ System.out.println(e.getStackTrace());
            return ResponseEntity.status(500).body("Something went wrong");
        }
    }

    public ResponseEntity<?> empbyid(String id){
        try{
            if(employeeRepository.findById(id).isPresent()){
                return ResponseEntity.ok().body(employeeRepository.findById(id));
            }
            return ResponseEntity.ok().body("id not found. Give a proper id");
        }catch (Exception e){ System.out.println(e.getStackTrace());
            return ResponseEntity.status(500).body("Something went wrong");
        }
    }

    public ResponseEntity<?> storeemp(Employee body){
        try {
            return ResponseEntity.ok().body(employeeRepository.save(body));
        }catch (Exception e){ System.out.println(e.getStackTrace());
            return ResponseEntity.status(500).body("Something went wrong");
        }
    }

    public ResponseEntity updateemp(Employee body) throws NullPointerException{
        if(employeeRepository.findById(body.getEmpId()).isPresent()){
            Employee emp = employeeRepository.findById(body.getEmpId()).get();
            emp.setGender(body.getGender());
            emp.setEmpStatus(body.getEmpStatus());
            emp.setDesignation(body.getDesignation());
            emp.setEmail(body.getEmail());
            emp.setName(body.getName());
            emp.setOfficeLocation(body.getOfficeLocation());
            emp.setAge(body.getAge());
            emp.setSalary(body.getSalary());
            employeeRepository.save(emp);
            return ResponseEntity.ok().body("Update Success");
        }
        return ResponseEntity.status(404).body("User not found. Please specify the proper ID for updation");
    }

    public ResponseEntity deleteemp(String id){
        try{
            if(employeeRepository.findById(id).isPresent()){
                employeeRepository.deleteById(id);
                return ResponseEntity.ok().body("Success");
            }
            return ResponseEntity.status(404).body("User not found");
        }catch (Exception e){
            System.out.println(e.getStackTrace());
            return ResponseEntity.status(500).body("Something went wrong");
        }

    }

    public ResponseEntity checkstat(){
        List<Employee> emps = employeeRepository.findAll();
        Map<String,List<String>> mp = new HashMap<>();
        try {
            for(Employee e:emps ){
                if (!mp.containsKey(e.getEmpStatus())) {
                    mp.put(e.getEmpStatus(), new ArrayList<String>());
                }
                mp.get(e.getEmpStatus()).add(e.getName());
            }
            return ResponseEntity.ok().body(mp);
        }catch (Exception e){ System.out.println(e.getStackTrace());
            return ResponseEntity.status(500).body("Something went wrong");
        }

    }
}
