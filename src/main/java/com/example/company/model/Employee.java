package com.example.company.model;

import jakarta.persistence.Entity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Document("Employee")
public class Employee {
    @Id
    private String empId;
    private String name;
    private String email;
    private String gender;
    private String designation;
    private String officeLocation;
    private String empStatus;
    private float salary;
    private int age;

    @Override
    public String toString() {
        return "Employee{" +
                "empId='" + empId + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", designation='" + designation + '\'' +
                ", officeLocation='" + officeLocation + '\'' +
                ", empStatus='" + empStatus + '\'' +
                ", salary=" + salary +
                ", age=" + age +
                '}';
    }

    //    public Employee() {
//    }

//    public Employee(String empId, String name, String email, String gender, String designation, String officeLocation, String empStatus, int age,float salary) {
//        this.empId = empId;
//        this.name = name;
//        this.email = email;
//        this.gender = gender;
//        this.designation = designation;
//        this.officeLocation = officeLocation;
//        this.empStatus = empStatus;
//        this.age=age;
//        this.salary=salary;
//    }

//    public float getSalary() {
//        return salary;
//    }
//
//    public void setSalary(float salary) {
//        this.salary = salary;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public String getEmpId() {
//        return empId;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public void setEmpId(String empId) {
//        this.empId = empId;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public String getDesignation() {
//        return designation;
//    }
//
//    public void setDesignation(String designation) {
//        this.designation = designation;
//    }
//
//    public String getOfficeLocation() {
//        return officeLocation;
//    }
//
//    public void setOfficeLocation(String officeLocation) {
//        this.officeLocation = officeLocation;
//    }
//
//    public String getEmpstatus() {
//        return empstatus;
//    }
//
//    public void setEmpstatus(String empstatus) {
//        this.empstatus = empstatus;
//    }
}
