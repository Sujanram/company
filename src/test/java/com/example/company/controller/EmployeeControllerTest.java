package com.example.company.controller;

import com.example.company.model.Employee;
import com.example.company.repository.EmployeeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.bson.assertions.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Test
    public void getAll() throws Exception {
        this.mvc.perform(get("/employee/all")).andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
    }

    @Test
    public void getById() throws Exception {
        this.mvc.perform(get("/employee/1")).andExpect(status().isOk());
//                .andExpect(content().contentType("application/json"));
    }

    @Test
    public void testAddEmployee() throws Exception {
        // Create a new Employee object
        Employee emp = new Employee("212","abc", "abc@gmail.com","m","sdf","dfsdf","active",12211,12);

        when(employeeRepository.save(any(Employee.class))).thenAnswer((Answer<Employee>) invocation -> {
            Employee employee = invocation.getArgument(0);
            employee.setEmpId("212");
            employee.setName("abc");
            employee.setEmail("abc@gmail.com");
            employee.setGender("m");
            employee.setDesignation("sdf");
            employee.setOfficeLocation("dfsdf");
            employee.setSalary(12211);
            employee.setEmpStatus("active");
            employee.setAge(12);
            return employee;
        });

        // Perform the POST request with the request body
        mvc.perform(post("/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(emp)))
                .andExpect(status().isOk()) ;

        verify(employeeRepository, times(1)).save(any(Employee.class));
    }

    @Test
    public void testUpdateEmployee() throws Exception {
        Employee emp = new Employee("212","abc", "abc@gmail.com","m","sdf","dfsdf","active",12211,12);
        emp.setEmpId("212");

        when(employeeRepository.findById("212")).thenReturn(Optional.of(emp));
        emp.setEmail("updated@example.com");
        emp.setAge(30);

        // Perform the POST request with the request body
        mvc.perform(put("/employee/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(emp)))
                .andExpect(status().isOk()) ;
        verify(employeeRepository, times(1)).save(any(Employee.class));
    }


    @Test
    public void testUpdateEmpNotFound() throws Exception {
        Employee employee = new Employee();
        employee.setEmpId("999");
        employee.setName("newName");

        mvc.perform(put("/employee/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(employee)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User not found. Please specify the proper ID for updation"));
    }

    private static String asJsonString(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }


    @Test
    public void testDeleteEmployee() throws Exception {
        String empId = "1";
        when(employeeRepository.findById(empId)).thenReturn(Optional.of(new Employee(empId, "anonymous", "anonymous@gmail.com", "Male", "SDE",  "Bangalore", "Active", 50000,22)));

        mvc.perform(delete("/employee/delete/" + empId))
                .andExpect(status().isOk())
                .andExpect(content().string("Success"));

        verify(employeeRepository, times(1)).deleteById(empId);
    }

    @Test
    public void testDeleteNonExistingEmployee() throws Exception {
        String empId = "100";
        when(employeeRepository.findById(empId)).thenReturn(Optional.empty());

        mvc.perform(delete("/employee/delete/" + empId))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User not found"));

        verify(employeeRepository, times(0)).deleteById(empId);
    }

    @Test
    public void testDeleteEmployeeException() throws Exception {
        String empId = "1";
        doThrow(new RuntimeException("Unable to delete employee")).when(employeeRepository).deleteById(empId);
        when(employeeRepository.findById(empId)).thenReturn(Optional.of(new Employee(empId, "anonymous", "anonymous@gmail.com", "Male", "SDE",  "Bangalore", "Active", 50000,22)));

        mvc.perform(delete("/employee/delete/" + empId))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Something went wrong"));

        verify(employeeRepository, times(1)).deleteById(empId);
    }

    @Test
    public void statTest() throws Exception {
        this.mvc.perform(get("/employee/stat")).andExpect(status().isOk());
//                .andExpect(content().contentType("application/json"));
    }

}
