package com.example.company.repository;

import com.example.company.model.Employee;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

//@RunWith(SpringRunner.class)
@DataJpaTest
@EnableMongoRepositories
@EnableAutoConfiguration
public class EmployeeRepositoryTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Before
    public void setUp(){
        // given
        Employee employee = Employee.builder()
                .empId("12")
                .name("fsd")
                .email("asda")
                .gender("m")
                .designation("fsd")
                .officeLocation("fgdfg")
                .empStatus("active")
                .salary(12312)
                .age(12)
                .build();

        testEntityManager.persist(employee);
    }



    @Test
    public void whenFindAll_thenReturnProductList() {
        // when
        List<Employee> products = employeeRepository.findAll();

        // then
        assertThat(products).hasSize(1);
    }

}
