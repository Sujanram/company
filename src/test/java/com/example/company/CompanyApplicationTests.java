package com.example.company;

import com.example.company.repository.EmployeeRepository;
import com.example.company.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@EnableAutoConfiguration
@EnableMongoRepositories
//		(basePackageClasses = EmployeeRepository.class)
//		(basePackages = "com.example.company.repository.EmployeeRepositoryTest.repository")
class CompanyApplicationTests {



	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private CompanyApplication app;

	@Test
	public void testMongoDbConnection() {
		// Check that we can connect to the MongoDB database
		assertEquals(true, mongoTemplate.collectionExists("Employee"));
	}
	@Test
	void contextLoads() {
	}
}
