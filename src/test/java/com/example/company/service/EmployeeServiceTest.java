package com.example.company.service;

import com.example.company.model.Employee;
import com.example.company.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    private Employee emp;


    @BeforeEach
    public void setup(){
        //employeeRepository = Mockito.mock(EmployeeRepository.class);
        //employeeService = new EmployeeServiceImpl(employeeRepository);
        emp = Employee.builder()
                .empId("1")
                .name("fsd")
                .email("asda")
                .gender("m")
                .designation("fsd")
                .officeLocation("fgdfg")
                .empStatus("active")
                .salary(12312)
                .age(12)
                .build();
    }

    @Test
    public void givenEmployeeObject_whenSaveEmployee_thenReturnEmployeeObject(){
        given(employeeRepository.save(emp)).willReturn(emp);
        ResponseEntity<?> savedemp = employeeService.storeemp(emp);
        assertEquals(savedemp.getBody(),emp);
    }


    @Test
    public void givenEmployeesList_whenGetAllEmployees_thenReturnEmployeesList(){
        List<Employee> expEmp = Arrays.asList(emp);
        doReturn(expEmp).when(employeeRepository).findAll();
        ResponseEntity<?> actualEmps = employeeService.allemp();
        assertThat(actualEmps.getBody()).isEqualTo(expEmp);
    }

    @Test
    public void givenEmployee_whenGetEmployeeById_thenReturnEmployee(){
        given(employeeRepository.findById("1")).willReturn(Optional.of(emp));
        ResponseEntity<?> savedEmployee = employeeService.empbyid(emp.getEmpId());
        assertThat(savedEmployee.getBody()).isNotNull();
    }

    @Test
    void givenEmployeeById_WhenIdDoesNotExist() {
        String id = "2";
        Optional<Employee> optional = Optional.empty();
        when(employeeRepository.findById(id)).thenReturn(optional);
        ResponseEntity<?> response = employeeService.empbyid(id);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("id not found. Give a proper id", response.getBody());
    }

    @Test
    void givenEmployeeById_WhenExceptionIsThrown() {
        String id = "3";
        when(employeeRepository.findById(id)).thenThrow(new RuntimeException("Something went wrong"));
        ResponseEntity<?> response = employeeService.empbyid(id);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Something went wrong", response.getBody());
    }


    @Test
    public void givenEmployeeId_DeleteEmployeeSuccess() {
        Employee employee = new Employee();
        employee.setEmpId("1");
        when(employeeRepository.findById("1")).thenReturn(Optional.of(employee));
        doNothing().when(employeeRepository).deleteById("1");
        String expectedMessage = "Success";
        String actualMessage = employeeService.deleteemp("1").getBody().toString();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void givenEmployeeId_DeleteEmployeeNotFound() {
        when(employeeRepository.findById(anyString())).thenReturn(Optional.empty());
        String expectedMessage = "User not found";
        String actualMessage = employeeService.deleteemp("1").getBody().toString();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void givenEmployeeId_DeleteEmployeeException() {
        when(employeeRepository.findById(anyString())).thenThrow(new RuntimeException());
        String expectedMessage = "Something went wrong";
        String actualMessage = employeeService.deleteemp("1").getBody().toString();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void givenEmployeeList_whenGetStat_returnCheckStat() {
        // mock employee data
        Employee employee1 = new Employee();
        employee1.setName("Sujan");
        employee1.setEmpStatus("Active");

        Employee employee2 = new Employee();
        employee2.setName("Ram");
        employee2.setEmpStatus("Inactive");

        List<Employee> employees = new ArrayList<>();
        employees.add(employee1);
        employees.add(employee2);

        // mock repository method
        when(employeeRepository.findAll()).thenReturn(employees);
        Map<String, List<String>> expectedResult = new HashMap<>();
        List<String> activeEmployees = new ArrayList<>();
        activeEmployees.add("Sujan");
        expectedResult.put("Active", activeEmployees);
        List<String> inactiveEmployees = new ArrayList<>();
        inactiveEmployees.add("Ram");
        expectedResult.put("Inactive", inactiveEmployees);

        // test the method
        ResponseEntity<?> response = employeeService.checkstat();
        Map<String, List<String>> result = (Map<String, List<String>>) response.getBody();

        // assert the result
        assert response.getStatusCodeValue() == 200;
        assert result.equals(expectedResult);
    }


    @Test
    public void givenEmployee_whenPutEmployee_returnUpdateEmployee() {
        Employee employee = new Employee();
        employee.setEmpId("1");
        employee.setGender("Male");
        employee.setEmpStatus("Active");
        employee.setDesignation("Manager");
        employee.setEmail("john.doe@example.com");
        employee.setName("John Doe");
        employee.setOfficeLocation("New York");
        employee.setAge(35);
        employee.setSalary(100000);

        when(employeeRepository.findById("1")).thenReturn(Optional.of(employee));

        Employee updatedEmployee = new Employee();
        updatedEmployee.setEmpId("1");
        updatedEmployee.setGender("Female");
        updatedEmployee.setEmpStatus("Inactive");
        updatedEmployee.setDesignation("Engineer");
        updatedEmployee.setEmail("jane.doe@example.com");
        updatedEmployee.setName("Jane Doe");
        updatedEmployee.setOfficeLocation("San Francisco");
        updatedEmployee.setAge(30);
        updatedEmployee.setSalary(80000);

        ResponseEntity response = employeeService.updateemp(updatedEmployee);

        verify(employeeRepository, times(1)).save(any(Employee.class));
        assertEquals(response.getBody(), "Update Success");
    }

    @Test
    public void givenEmployee_whenPutEmployee_returnUpdateNonExistentEmployee() {
        Employee employee = new Employee();
        employee.setEmpId("2");
        employee.setGender("Male");
        employee.setEmpStatus("Active");
        employee.setDesignation("Manager");
        employee.setEmail("sujan@ram.com");
        employee.setName("Sujan");
        employee.setOfficeLocation("Bengaluru");
        employee.setAge(35);
        employee.setSalary(100000);

        when(employeeRepository.findById("2")).thenReturn(Optional.empty());

        ResponseEntity response = employeeService.updateemp(employee);

        verify(employeeRepository, times(1)).findById("2");
        verify(employeeRepository, never()).save(any(Employee.class));
        assertEquals(response.getStatusCodeValue(), 404);
        assertEquals(response.getBody(), "User not found. Please specify the proper ID for updation");
    }

}
